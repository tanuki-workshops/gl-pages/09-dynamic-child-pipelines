class Job {
  constructor({name, script, artifacts, rules, environment, allow_failure}) {
    this[name] = {}
    if (script!==undefined) this[name].script = script
    if (artifacts!==undefined) this[name].artifacts = artifacts
    if (rules!==undefined) this[name].rules = rules
    if (environment!==undefined) this[name].environment = environment
    if (allow_failure!==undefined) this[name].allow_failure = allow_failure
  }
}

class Artifacts {
  constructor({paths}) {
    this.paths = paths
  }
}

class Rules {
  constructor(rules) {
    this.list = rules
  }
}

class Environment {
  constructor({name, url, on_stop, action}) {
    this.name = name
    if(url!==undefined) this.url = url
    if(on_stop!==undefined) this.on_stop = on_stop
    if(action!==undefined) this.action = action
    
  }
}

module.exports = {
  Job, Artifacts, Rules, Environment
}
