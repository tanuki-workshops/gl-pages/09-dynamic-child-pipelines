const fs = require("fs")
const yaml = require('js-yaml')

const ci = require('./ci.helpers.js')

commands = [
    `mkdir -p public/{js,css}`
  , `cp ./website/js/*.* public/js`
  , `cp ./website/css/*.* public/css`
  , `cp ./website/index.html public`
]

let artifacts = new ci.Artifacts({paths:["public"]})

let rules = new ci.Rules([
  {if: `$CI_COMMIT_BRANCH == "master"`}
])

let environment = new ci.Environment({
    name: "production/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}"
  , url: "https://tanuki-workshops.gitlab.io/gl-pages/${CI_PROJECT_NAME}"
})

let pagesJob = new ci.Job({
     name: "pages"
  ,  script: commands
  ,  artifacts: artifacts
  ,  rules: rules.list
  ,  environment: environment
})

let yamlJob = yaml.safeDump(pagesJob)

fs.writeFileSync('./production.publication.yml', yamlJob, 'utf8')

