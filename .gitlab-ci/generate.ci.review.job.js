const fs = require("fs")
const yaml = require('js-yaml')
const ci = require('./ci.helpers.js')

commands = [
    `mkdir -p public/{js,css}`
  , `cp ./website/js/*.* public/js`
  , `cp ./website/css/*.* public/css`
  , `cp ./website/index.html public`
]

let artifacts = new ci.Artifacts({paths:["public"]})

let rules = new ci.Rules([
  {if: `$CI_MERGE_REQUEST_IID`}
])

let environment = new ci.Environment({
    name: "preview/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}"
  , url: "https://tanuki-workshops.gitlab.io/-/gl-pages/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/public/index.html"
  //, on_stop: `😢pages:preview:stop`
})

let previewPagesJob = new ci.Job({
     name: "🤔pages:preview"
  ,  script: commands
  ,  artifacts: artifacts
  ,  rules: rules.list
  ,  environment: environment
})

let yamlJob = yaml.safeDump(previewPagesJob)

fs.writeFileSync('./review.publication.yml', yamlJob, 'utf8')

/* --- store variable (it will allow to remove the review environment at the start of the production pipeline) --- */
fs.writeFileSync('./variables.store.json', JSON.stringify({
  ciProjectName: process.env.CI_PROJECT_NAME,
  ciCommitRefName: process.env.CI_COMMIT_REF_NAME
}), 'utf8')


