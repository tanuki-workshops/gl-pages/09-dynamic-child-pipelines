const fs = require("fs")
const yaml = require('js-yaml')
const ci = require('./ci.helpers.js')

commands = [
  `echo "👋 bye"`
]

let rules = new ci.Rules([
  {if: "$CI_MERGE_REQUEST_IID", when: "manual"}
])

let environment = new ci.Environment({
    name: "preview/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}"
  , action: "stop"
})

let removePreviewPagesJob = new ci.Job({
     name: "😢pages:preview:stop"
  ,  script: commands
  ,  rules: rules.list
  ,  environment: environment
  ,  allow_failure: true
})

let yamlJob = yaml.safeDump(removePreviewPagesJob)

fs.writeFileSync('./drop.review.yml', yamlJob, 'utf8')

